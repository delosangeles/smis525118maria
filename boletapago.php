<?php 
$horas = $_POST['horas'];
$salarioEmpleado = $_POST['salario'];

class Boletapago{
   
    private $sueldon;
    private $isss;
    private $afp;
    private $renta;
    private $descuentos;
    private $hextras;
    private $salariototal;


    public function getSueldoN():float{
        return $this->sueldon;
    }	
    public function setSueldoN(float $SALARIO){
        $this->sueldon=$SALARIO;
    }	

    public function getISSS():float{
        return $this->isss;
    }	
    public function setISSS(float $ISSS){
        $this->isss=$ISSS;
    }	
    
    public function getAFP():float{
        return $this->afp;
    }	
    public function setAFP(float $AFP){
        $this->afp=$AFP;
    }	

    public function getRENTA():float{
        return $this->renta;
    }	
    public function setRENTA(float $RENTA){
        $this->renta=$RENTA;
    }

    public function getDESCUENTOS():float{
        return $this->descuentos;
    }	

    public function setDESCUENTOS(float $Total){
        $this->descuentos=$Total;
    }
 
    public function getExtras():float{
        return $this->hextras;
    }	

    public function setExtras(float $horas){
        $this->hextras=$horas;
    }

    public function getTSalario():float{
        return $this->salariototal;
    }	

    public function setTsalario(float $TSALARIO){
        $this->salariototal=$TSALARIO;
    }
    ///////////////////////////////////////////////////////////////////////////////////

    ///Funciones
    public function descuentoisss(float $ISSS){
        $descuentoISSS = $this->sueldon * $ISSS ;
        $this->setISSS($descuentoISSS);
    }

    public function descuentoafp(float $AFP){
        $descuentoAFP = $this->sueldon * $AFP ;
        $this->setAfp($descuentoAFP);
    }

    public function totaldescuento(){
        $tdescuento = $this->afp + $this->isss + $this->renta;
        $this->setDESCUENTOS($tdescuento);
    }
    
  
    public function descuentoreta(){
        $renta = 0;
        $coutafija = 0;
        $exceso = 0;

        if($this->sueldon >=0.01 && $this->sueldon <=472.00){
            $renta = 0;
            $coutafija = 0;
            $retencion = 0;
        }else if($this->sueldon >=472.01 && $this->sueldon<=895.24){
            $coutafija = 17.67;
            $retencion = ($this->sueldon - 472) * 0.1;
            $renta = $retencion + $coutafija;
        }else if($this->sueldon >=895.25 && $this->sueldon <=2038.10){
            $coutafija = 60;
            $retencion = $this->sueldon - 895.25 * 0.2;
            $renta = $retencion + $coutafija;   
        }else{
            $coutafija = 288.57;
            $retencion = $this->sueldon - 2038.11 * 0.3;
            $renta = $retencion + $coutafija;   
        }
        $this->setRENTA($renta);
    }

    public function horasextras(float $horas){
        $hora = 10;
        $pagoh = $horas * $hora;
        $this->setExtras($pagoh);
    }

    
    public function salarioliquido(){
        $tsueldo = $this->sueldon + $this->hextras - $this->descuentos;
        $this->setTsalario($tsueldo);
    }
    function __destruct(){
        echo "\n<p class=\"msg\"></p>\n";
        echo "\n<p>\n<a href=\"datoempleado.php\">Salario de otro empleado</a>\n</p>\n";
        }
    
}
$empleado = new Boletapago();

$empleado->setSueldoN($salarioEmpleado);
$empleado->descuentoisss(0.075);
$empleado->descuentoafp(0.035);
$empleado->horasextras($horas);
$empleado->descuentoreta(); 
$empleado->totaldescuento();
$empleado->salarioliquido();
echo '
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Boleta Pago</title>

<style>
.col-md-12{
    font-size:35px;
}
</style>
<link type="text/css" rel="stylesheet" href="empleado.css" media="screen" />
</head>
';
echo '
<body>
<h1 class="text-center mt-4">Boleta de Pago Empleado</h1>
  <div class="container mt-3">
    <div class="row">
    <div class="col-md-12">';
   
   
    echo "<b>Salario  $ </b> " , $empleado->getSueldoN();
    echo "</br> <b> Pago Horas Extras $ </b> " , $empleado->getExtras();
    echo"</br> <b> Descuentos </b> " ;
    echo "</br> <b>Descuento ISSS $</b> ",$empleado->getISSS();
    echo "</br> <b>Descuento AFF $</b>" , $empleado->getAFP();
    echo "</br> <b> Descuento de RENTA $</b> " , $empleado->getRENTA();
    echo "</br> <b> Total Descuentos $</b> " , $empleado->getDESCUENTOS();
   
    echo "</br> <b> Total Sueldo Liquido </b> " , $empleado->getTSalario(); 
    echo '</div>
    </div>
  </div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>';
?>


